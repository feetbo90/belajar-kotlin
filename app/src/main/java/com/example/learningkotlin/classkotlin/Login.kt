package com.example.learningkotlin.classkotlin

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Login(
    @SerializedName("status")
    @Expose
    val status: Int
):Parcelable