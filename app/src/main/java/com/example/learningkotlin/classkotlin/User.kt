package com.example.learningkotlin.classkotlin

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("firstname")
    @Expose
    val firstname: String,
    @SerializedName("lastname")
    @Expose
    val lastname: Int,
    @SerializedName("email")
    @Expose
    val email: String
): Parcelable