package com.example.learningkotlin.network

import com.example.learningkotlin.classkotlin.Login
import com.example.learningkotlin.classkotlin.User
import retrofit2.Call
import retrofit2.http.*

interface ApiClient {
    @GET("android/login.php")
    //@FormUrlEncoded
    fun checkLogin(@Query("username") username: String,
                   @Query("password") password: String)
            : Call<Login>

    @FormUrlEncoded
    @POST("android/register.php")
    fun createUser(@Field("username") username: String,
                   @Field("password") password: String,
                   @Field("firstname") firstname: String,
                   @Field("lastname") lastname: String,
                   @Field("email") email: String) :
            Call<Login>

    @GET("/posts")
    fun getData(): Call<List<User>>
}