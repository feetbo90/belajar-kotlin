package com.example.learningkotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.learningkotlin.classkotlin.Login
import com.example.learningkotlin.network.RestClient
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val postService = RestClient.DataRepository.creating()

        register.setOnClickListener {
            val pindahKeRegister = Intent(applicationContext, RegisterActivity::class.java)
            startActivity(pindahKeRegister)
        }

        signin1.setOnClickListener {
            var username = editUsername.text.toString()
            var password = editPass.text.toString()

            postService.checkLogin(username, password).enqueue(object : Callback<Login>{
                override fun onFailure(call: Call<Login>, t: Throwable) {
                    Toast.makeText(this@LoginActivity, "${t.message}", Toast.LENGTH_LONG).show()

                }

                override fun onResponse(call: Call<Login>, response: Response<Login>) {
                    val data = response.body()
                    val status = data!!.status
                    if(status == 1) {
                        val pindahHalaman = Intent(applicationContext, DataActivity::class.java)
                        startActivity(pindahHalaman)
                    }
                    Toast.makeText(this@LoginActivity, data!!.status, Toast.LENGTH_LONG).show()

                }

            })

        }
    }
}
