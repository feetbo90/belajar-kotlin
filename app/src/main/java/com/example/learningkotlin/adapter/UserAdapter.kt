package com.example.learningkotlin.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.example.learningkotlin.R
import com.example.learningkotlin.classkotlin.User

class UserAdapter(User: List<User>, context : Context) : RecyclerView.Adapter<UserAdapter.MyViewHolder>() {

    private var User : List<User>? = null
    private var context : Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): UserAdapter.MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rowfoul, parent, false)
        return MyViewHolder(v, this.context)
    }

    override fun getItemCount(): Int = this.User!!.size


    override fun onBindViewHolder(holder: UserAdapter.MyViewHolder, position: Int) {
        val result  = this.User!![position].firstname

        holder.pelajaran.text = result//context!!.getString(R.string.username)
        holder.nilai.text = this.User!![position].lastname.toString()
        holder.tgl_mulai.text = this.User!![position].id.toString()
        //holder.tgl_selesai.text = this.User!![position].tgl_target
        holder.statusTugas.text = this.context!!.getString(R.string.more)
        //this.User!![position].status_tugas

        var letter = "A"


        if (!result.isEmpty()) {
            letter = result.substring(0, 1)
        }

        val mColorGenerator: ColorGenerator = ColorGenerator.DEFAULT
        val color = mColorGenerator.randomColor
        val mDrawableBuilder: TextDrawable
        // Create a circular icon consisting of  a random background colour and first letter of title
        mDrawableBuilder = TextDrawable.builder()
            .buildRound(letter, color)
        holder.mThumbnailImage.setImageDrawable(mDrawableBuilder)
    }

    init {
        this.User = User
        this.context = context
    }

    class MyViewHolder(itemView: View, context: Context?) : RecyclerView.ViewHolder(itemView) {


        val pelajaran: TextView = itemView.findViewById(R.id.textName) as TextView
        val nilai: TextView = itemView.findViewById(R.id.textAlamat) as TextView
        val tgl_mulai: TextView = itemView.findViewById(R.id.tglmulai) as TextView
        //val tgl_selesai: TextView = itemView.findViewById(R.id.tglselesai)as TextView
        val statusTugas: TextView = itemView.findViewById(R.id.tugasMateri)as TextView

        val mThumbnailImage: ImageView = itemView.findViewById<View>(R.id.thumbnail_image) as ImageView
        init {
            val medium = Typeface.createFromAsset(context!!.assets, "fonts/MontserratMedium.ttf")
            pelajaran.typeface = medium

            itemView.setOnClickListener {
                val value = adapterPosition
                callback?.onSuggestResult(value)
                //onItemClickListener = itemClickListener
                //onItemClickListener?.onItemClick(value)
            }

        }
    }

    interface onSuggestResultListener {
        fun onSuggestResult(posisi : Int)
    }

    private companion object {
        var callback: UserAdapter.onSuggestResultListener? = null
    }

    fun setOnSuggestResultListener(x: onSuggestResultListener) {
        callback = x
    }

}