package com.example.learningkotlin

class hewan{
    // atribut
    var kaki: Int? = null
    var warna: String? = null
    var jk: String? = null

    // ciri-ciri
    fun bergerak() {
        println("hewan bergerak")
    }

    fun bernafas() {
        println("hewan bernafas")
    }
}