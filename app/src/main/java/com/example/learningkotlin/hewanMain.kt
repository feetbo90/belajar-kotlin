package com.example.learningkotlin

import com.example.learningkotlin.animals.hewan2

fun main() {

    // constructor I
    val sapi = hewan2(4, "Hitam", "Pr")


    sapi.bergerak()
    sapi.bernafas()

    // constructor II
    val kambing = hewan2(3, 4, "hitam putih", "Lk")
    kambing.bergerak()
}

