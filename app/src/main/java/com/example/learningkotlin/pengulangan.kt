package com.example.learningkotlin

fun main() {

    // memakai until
//    for (i=1 ; i<10;i++)
    // memakai until
    for(i in 1 until 11) {
        println(i)
    }

    println("===================")
    for (nilai in 20 downTo 1)
    {
        println(nilai)
    }

    println("===================")
    for (nilai2 in 20 downTo 0 step 5)
    {
        println(nilai2)
    }

    println("===================")
    for (nilai3 in 0..20  step 5) {
        println(nilai3)
    }

    println("===================")

    val bil1 = 10
    val bil2 = 10
    val hasilKurang = bil1 - bil2
    println("hasil jumlah : ${bil1 + bil2}")
    println("nilai bil 1 : $bil1")
    println("hasil kurang : $hasilKurang")
}