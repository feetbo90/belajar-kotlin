package com.example.learningkotlin

fun main() {

//    array 4
//    nilai    10 9 8 7
//    index     0 1 2 3

    var list = mutableListOf<Int>()

    list.add(10)
    list.add(1, 9)
    list.add(2, 7)
    list.add(3, 8)
    for (i in 0 until list.size){
        println(list[i])
    }
    println("==========================")
    list.removeAt(3)
    for (i in 0 until list.size){
        println(list[i])
    }

    // immutable
    var list2 = listOf<Int>( 2,3,4,5)
    println("==========================")

    for (element in list2){
        println(element)
    }
    println("==========================")

}