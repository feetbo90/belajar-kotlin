package com.example.learningkotlin

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.learningkotlin.classkotlin.Login
import com.example.learningkotlin.network.RestClient
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val getRegister = RestClient.DataRepository.creating()
        register.setOnClickListener {
            val username = editUsername.text.toString()
            val password = editPass.text.toString()
            val firstname = editFirstname.text.toString()
            val lastname = editLastName.text.toString()
            val email = editEmail.text.toString()

            getRegister.createUser(username, password, firstname,
                lastname, email).enqueue(object : Callback<Login>{
                override fun onFailure(call: Call<Login>, t: Throwable) {

                }

                override fun onResponse(call: Call<Login>, response: Response<Login>) {
                    val status = response.body()!!.status
                    if(status == 1) {

                        Toast.makeText(applicationContext, "Berhasil Login", Toast.LENGTH_LONG).show()

                    }
                }


            })

    }
}
}
