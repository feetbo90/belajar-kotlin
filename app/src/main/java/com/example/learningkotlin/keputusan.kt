package com.example.learningkotlin

fun main() {
    // true
    if(2 > 4 || 3 > 1) {
        println("benar")
    }
    else {
        println("salah")
    }

}

/*
operator logika
AND && OR ||

&&
T   T   T
T   F   F
F   T   F
F   F   F

||
T   T   T
T   F   T
F   T   T
F   F   F
 */