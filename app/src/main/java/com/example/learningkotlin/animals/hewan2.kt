package com.example.learningkotlin.animals

class hewan2(kaki: Int, warna: String, jk: String){
    // atribut
    var kaki1: Int? = null
    var warna1: String? = null
    var jk1: String? = null
    var umur1: Int? = 1

    init {
        this.kaki1 = kaki
        this.warna1 = warna
        this.jk1 = jk
    }

    // constructor II harus memanggil I
    constructor(umur: Int, kaki2: Int, warna2:String, jk2:String) :
            this(kaki2, warna2, jk2) {
        this.umur1 = umur
    }

    // ciri-ciri
    fun bergerak() {
        println("hewan dengan umur ${this.umur1}")
    }

    fun bernafas() {
        println("hewan bernafas")
    }
}