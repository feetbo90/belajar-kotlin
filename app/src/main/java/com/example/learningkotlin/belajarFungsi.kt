package com.example.learningkotlin
import java.util.Scanner


// tipe pengembalian nilai
fun pengurangan(bil1: Int, bil2: Int): Int {
    val hasil = bil1 - bil2
    val hasil2 = bil1 * bil2
    return hasil
}

// fungsi expression
fun perkalian(bil1: Int, bil2: Int) = bil1 * bil2

//fun hasilNilai(nilai: Int) = if (nilai >= 90) {
////    "A"
////}

// fungsi named parameter
fun pembagian(bil1: Int = 12, bil2: Int) =
    bil1.toDouble()/ bil2.toDouble()

// tanpa pengembalian nilai
fun penjumlahan(bilangan1: Int, bilangan2: Int) {
    val hasil = bilangan1 + bilangan2
    println(hasil)
}

// fungsi extension
fun Int.modulo(bil2: Int) : Int {
    return this % bil2
}

// fungsi infix

fun main() {
    val input = Scanner(System.`in`)
    println("Masukkan bilangan 1 : ")
    var bil1 = input.nextInt()
    println("Masukkan bilangan 2 : ")
    var bil2 = input.nextInt()

    penjumlahan(bil1, bil2)
    // 4
    // 5
    val bagi = pembagian(bil1, bil2)
    val bagi2 = pembagian(bil2 = bil2)
    println("ini pembagian yang pertama $bagi")
    println("ini pembagian yang kedua $bagi2")
    // bil1 = 10
    // bil2 = 20
    val modulo = bil1.modulo(bil2)
    val kurang = bil1 dikurang bil2
}

infix fun Int.dikurang(bil2: Int) : Int {
    return this - bil2
}
